const fs = require("fs");
const path = require("path")

function problem2(id, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname,"./data/lists.json"),'utf-8',(err, data) => {
            if (err) {
                console.log(err);
            } else {
                let result = JSON.parse(data)
                let listID = result[id]
                if(listID){
                    callback(null, listID)
                } else {
                    callback(new Error("List not found"))
                }
            }

        })
    }, 2 * 1000)
}


module.exports = problem2;