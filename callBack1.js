const fs = require("fs");
const path = require("path")

function problem1(id, callback){
    setTimeout(() =>{
        fs.readFile(path.join(__dirname,"./data/boards.json"),'utf-8',(err, data)=>{
            if(err){
                console.log(err);
            } else {
                // console.log("File Accessed");
                let result = JSON.parse(data)
                let boardID = result.find((item)=>{
                    return item.id == id
                })
                if(boardID){
                    callback(null,data);
                } else {
                    callback(new Error ("Board not found"))
                }
            }
        })

    },2 * 1000);
}

module.exports = problem1;
// console.log(problem1("mcu453ed", (err,data) =>{
//     if(err){
//         console.log(err);
//     } else {
//         console.log(data);
//     }
// }));