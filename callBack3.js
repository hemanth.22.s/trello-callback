const fs = require("fs");
const path = require("path")

function problem3(id, callback) {
    setTimeout(() => {
        fs.readFile(path.join(__dirname,"./data/cards.json"),'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let result = JSON.parse(data)
                // let Did = id
                // let resultArr =
                let cardID = result[id]
                if(cardID){
                    callback(null, cardID)
                } else {
                    callback(new Error("card not found"))
                }
            }

        })
    }, 2 * 1000)
}


module.exports = problem3