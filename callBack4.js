const fs = require("fs");
const path = require("path");
const callBackOne = require("./callBack1");
const callBackTwo = require("./callBack2");
const callBackThree = require("./callBack3");

function problem4(name){
    setTimeout(()=>{
        fs.readFile(path.join(__dirname,"./data/boards.json"),'utf-8',(err,data)=>{
            if(err){
                console.log(err);
            } else {
                // console.log(data);
                let res = JSON.parse(data).filter((index)=>{
                    return index.name == name;
                    // console.log(index.name);
                })
                let id = res[0].id;
                // console.log(id);
                callBackOne(id, (err,data)=>{
                    if(err){
                        console.log(err);
                    } else {
                        console.log(data);

                        callBackTwo(id, (err,data)=>{
                            if(err){
                                console.log(err);
                            } else {
                                console.log(data);

                                let mindData = data.filter((index)=>{
                                    return index.name == "Mind"
                                });

                                let mindDataId = mindData[0].id;

                                callBackThree(mindDataId,(err,data)=>{
                                    if(err){
                                        console.log(err);
                                    } else {
                                        console.log(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })

    },2 * 1000);
}

module.exports = problem4;

// console.log(problem4("Thanos"));

